﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SpawnScript : MonoBehaviour {
    // Cube element to spawn
    public GameObject mCubeObj;

    // Qtd of Cubes to be Spawned
    public int mTotalCubes = 10;

    // Time to spawn the Cubes
    public float mTimeToSpawn = 1f;

    // hold all cubes on stage
    private GameObject[] mCubes;

    // define if position was set
    private bool mPositionSet;
    
    private IEnumerator SpawnLoop()
    {
        StartCoroutine(ChangePosition());
        yield return new WaitForSeconds(0.2f);
        int i = 0;
        while (i <= (mTotalCubes - 1))
        {
            mCubes[i] = SpawnElement();
            i++;
            yield return new WaitForSeconds(Random.Range(mTimeToSpawn, mTimeToSpawn * 3));


        }
    }

    private GameObject SpawnElement()
    {
        GameObject cube = Instantiate(mCubeObj, (Random.insideUnitSphere * 4) + transform.position, transform.rotation) as GameObject;
        // define a random scale for the cube
        float scale = UnityEngine.Random.Range(0.5f, 2f);
        // change the cube scale
        cube.transform.localScale = new Vector3(scale, scale, scale);
        return cube;
    }

    private bool SetPosicion()
    {
        Transform cam = Camera.main.transform;
        transform.position = cam.forward * 10;
        return true;

    }
	// Use this for initialization
	void Start () {
        StartCoroutine(SpawnLoop());
        mCubes = new GameObject[mTotalCubes];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private IEnumerator ChangePosition()
    {
        yield return new WaitForSeconds(0.2f);
        if (!mPositionSet)
        {
            if (VuforiaBehaviour.Instance.enabled)
            {
                SetPosicion();
            }
        }
    }
}
