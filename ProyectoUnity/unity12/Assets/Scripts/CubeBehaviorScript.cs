﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBehaviorScript : MonoBehaviour {
    public float mScaleMax = 2f;
    public float mScaleMin = 0.5f;
    public int mCubeHealth = 100;
    // Orbit max Speed
    public float mOrbitMaxSpeed = 30f;

    // Orbit speed
    private float mOrbitSpeed;

    // Anchor point for the Cube to rotate around
    private Transform mOrbitAnchor;

    // Orbit direction
    private Vector3 mOrbitDirection;

    // Max Cube Scale
    private Vector3 mCubeMaxScale;

    // Growing Speed
    public float mGrowingSpeed = 10f;
    private bool mIsCubeScaled = false;

    private bool mIsAlive = true;

    // Cube got Hit
    // return 'false' when cube was destroyed
    public bool Hit(int hitDamage)
    {
        mCubeHealth -= hitDamage;
        if (mCubeHealth >= 0 && mIsAlive)
        {
            StartCoroutine(DestroyCube());
            return true;
        }
        return false;
    }
    private IEnumerator DestroyCube()
    {
        mIsAlive = false;

        // Make the cube desappear
        GetComponent<Renderer>().enabled = false;

        // we'll wait some time before destroying the element
        // this is usefull when using some kind of effect
        // like a explosion sound effect.
        // in that case we could use the sound lenght as waiting time
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
    // Use this for initialization
    void Start () {
        CubeSettings();
    }

    private void CubeSettings()
    {
        mOrbitAnchor = Camera.main.transform;

        // defining the orbit direction
        float x = UnityEngine.Random.Range(-1f, 1f);
        float y = UnityEngine.Random.Range(-1f, 1f);
        float z = UnityEngine.Random.Range(-1f, 1f);
        mOrbitDirection = new Vector3(x, y, z);

        // defining speed
        mOrbitSpeed = UnityEngine.Random.Range(5f, mOrbitMaxSpeed);

        // defining scale
        float scale = UnityEngine.Random.Range(mScaleMin, mScaleMax);
        mCubeMaxScale = new Vector3(scale, scale, scale);

        // set cube scale to 0, to grow it lates
        transform.localScale = Vector3.zero;

    }

    // Update is called once per frame
    private void Update () {
        RotateCube();
        if (!mIsCubeScaled)
        {
            ScaleObj();
        }
	}

    private void ScaleObj()
    {
        if (transform.localScale != mCubeMaxScale)
            transform.localScale = Vector3.Lerp(transform.localScale, mCubeMaxScale, Time.deltaTime * mGrowingSpeed);
        else
            mIsCubeScaled = true;
    }

    private void RotateCube()
    {
        transform.RotateAround(mOrbitAnchor.position,mOrbitDirection,mOrbitSpeed*Time.deltaTime);
        transform.Rotate(mOrbitDirection * 30 * Time.deltaTime);
    }
}
